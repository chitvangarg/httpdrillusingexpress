const express = require("express")
const fs = require("fs")
const uuid = require("uuid4")
const { port } = require("./config")
const app = express()

// server Creation at specified port
app.listen(port, () => {
    console.log(`Successfully created server at PORT: ${port}`)
})

// Get request at /html enpoint and responded with html file
app.get("/html", (req, res) => {
    res.sendFile(`${__dirname}/index.html`)
})

//Get request at /json endpoint and responded with JSON data
app.get("/json", (req, res) => {
    try {
        fs.readFile(`${__dirname}/data.json`, "utf-8", (err, data) => {
            if (err) {
                throw new Error("unable to read file")
            } else {
                const jsonData = JSON.parse(data)
                res.json(jsonData)
            }
        })
    } catch (err) {
        console.log(err.message)
    }
})

//Get request at /uuid endpoint and responded with uid object
app.get("/uuid", (req, res) => {
    const uidObject = {
        uuid: uuid(),
    }
    res.write(JSON.stringify(uidObject))
    res.end()
})

// Get request at /status/code responded with code id in URL params
app.get("/status/:status_code", (req, res) => {
    const code = req.params.status_code
    res.status(code)
    res.write(`Return a response with ${code} status code`)
    res.end()
})

// Get request at /delay/delay_in_sec responded with code id in URL params
app.get("/delay/:delay_in_seconds", (req, res) => {
    const delayTime = req.params.delay_in_seconds

    setTimeout(() => {
        res.status(200)
        res.write(`response with status code 200`)
        res.end()
    }, delayTime * 1000)
})
